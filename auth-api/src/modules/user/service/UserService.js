import UserRepository from "../repository/UserRepository.js";
import * as httpStatus from "../../../config/constants/HttpStatus.js"
import userExption from "../exception/UserExeption.js";

class UserService {

    async findByEmail(req) {

        try {
            const { email } = req.params;
            this.validated(email);
            let user = await UserRepository.findByEmail(email);
            this.validateUser(user);

            return {
                id: user.id,
                name: user.name,
                email: user.email
            }

        } catch (error) {
            return {
                status: error.status ? error.status : httpStatus.INTERNAL_SERVER_ERROR,
                message: error.status,
            }
        }

    }

    validated() {
        if (!email) {
            throw new userExption(httpStatus.BAD_REQUEST, "User email not informed.")
        }
    }

    validateUser(user){
        if(!user){
            throw new userExption(httpStatus.BAD_REQUEST, "User was not found")
        }
    }

}



export default new UserService();