import Sequelize from "sequelize";

const sequelize = new Sequelize("auth-db", "postgres", "1y5h8j", 
{
    host: "localhost",
    dialect: "postgres",
    quoteIdentifiers: false,
    define:{
        ayncOnAssociation: true,
        timestamps: false,
        underscored: true,
        underscoredAll: true,
        freezerTableName: true
    }
}
)
sequelize.authenticate()
    .then(() =>{
        console.log("Connection has been stablished");
    })
    .catch( (err)=>{
        console.error("Unable to connect to the databese");
        console.error(err.message);
    })

    export default sequelize;