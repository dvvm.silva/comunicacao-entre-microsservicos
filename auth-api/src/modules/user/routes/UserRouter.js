import { Router } from "express";
import UserControler from "../controller/UserControler.js";

const router = new Router();

router.get('/api/user/email/:email', UserControler.findByEmail);

export default router;