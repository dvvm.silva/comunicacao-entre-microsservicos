import express from "express";
import userRouter from "./src/modules/user/routes/UserRouter.js"

const app = express();
const env = process.env;
const PORT = env.PORT || 8080;

app.use(express.json);
app.use(userRouter);

app.get('/api/status', (req, res) =>{
    return res.json({
        service: "Auth-API",
        status: "UP",
        httpStatus: 200
    })
});
app.listen(PORT, ()=>{
    console.info(`server started successfully at port ${PORT}`)
})